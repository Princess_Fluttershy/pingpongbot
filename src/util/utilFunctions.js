const moment = require("moment");
const fs = require("fs");
const path = require("path");

const databasePath = path.join(__dirname, "..", "storage");
const logPath = path.join(databasePath, "log");
if(!fs.existsSync(databasePath)) fs.mkdirSync(databasePath);

module.exports.RunDate = moment().format("YYYY-MM-DD");
module.exports.RunUptime = moment().format("x");
module.exports.config = JSON.parse(fs.readFileSync(path.join(__dirname, "..", "settings", "config.json"), "UTF-8"));
module.exports.login = JSON.parse(fs.readFileSync(path.join(__dirname, "..", "settings", "login.json"), "UTF-8"));
const pingCountPath = path.join(__dirname, "..", "storage", "pingCount.json");
module.exports.pingCount = fs.existsSync(pingCountPath) ? JSON.parse(fs.readFileSync(pingCountPath, "UTF-8")) : {};
if(!this.pingCount["user"]) this.pingCount["user"] = {};
if(!this.pingCount["guilds"]) this.pingCount["guilds"] = {};

/**
 * Log Actions (Usages, Log, Safety, Debug, etc.)
 * @param {string|array} type
 * @param {string} msg
 * @param {*} options
 *
 * @example
 * log("Eval using by ExampleUser in Example") // Logs "CurrentDate: Eval using by ExampleUser in Example" into the current logfile
 * log("Debug: Error in XY", "debug") // Logs "CurrentDate: Debug: Error in XY" into the current debug-file
 */
module.exports.log = (msg, type = "run", ...options)=>{
    let date = moment().format("YYYY MMMM DD, [Time:] HH:mm:ss (Z)");
    if(!fs.existsSync(logPath)) fs.mkdirSync(logPath);
    let curLogPath = path.join(logPath, this.RunDate + "–");
    if(!Array.isArray(type)) type = [type];
    if(type.length >= 3) fs.appendFileSync(curLogPath + "run.log", "\n" + date + ": " + "!Warning!: Writing into " + type.length + " Logfile's on one Log.", "UTF-8");
    let content = "\n" + date + ": " + msg;

    //if(type.indexOf("debug") > -1) type.push("run"); // debug is only debug and doesn't show up in the run log
    let logged = [];
    for(let logType of type){
        if(typeof logType !== "string") logType = "run";
        if(logged.indexOf(logType) < 0){
            let currentPath = curLogPath + logType.toLowerCase() + ".log";
            fs.appendFileSync(currentPath, content, "UTF-8");
            logged.push(logType);
        }
    }
};

/**
 * Gives a formatted time output
 * @param {int} ms milliseconds
 * @return {string}
 */
module.exports.formatTime = (ms = (moment().format("x") - this.RunUptime))=>{
    let time = "";
    if(ms >= 8.64e+7){ // 24 Stunden (Tage)
        time += Math.floor(ms/8.64e+7) + "days ";
        ms = ms%8.64e+7;
    }else{
        time += "0 days, ";
    }
    if(ms >= 3.6e+6){ // Stunden
        let num = Math.floor(ms/3.6e+6);
        time += (num > 9) ? num : "0" + num + ":";
        ms = ms%3.6e+6;
    }else{
        time += "00:";
    }
    if(ms >= 60000){ // Minuten
        let num = Math.floor(ms/60000);
        time += (num > 9) ? num : "0" + num + ":";
        ms = ms%60000;
    }else{
        time += "00:";
    }
    if(ms >= 1000){ // Minuten
        let num = Math.floor(ms/1000);
        time += (num > 9) ? num : "0" + num + ":";
        ms = ms%1000;
    }else{
        time += "00";
    }
    return time + " " + ms + "ms";
};

/**
 * Adds +1 to a counter (type)
 * @param {id|string} userID
 * @param {id|string} serverID
 * @param {string} type
 */
module.exports.pingUp = (userID, serverID, type = "pings")=>{
    let time = moment().format("x");
    if(!this.pingCount["user"][userID]) this.pingCount["user"][userID] = {};
    (this.pingCount["user"][userID][type]) ? this.pingCount["user"][userID][type].push(time) : this.pingCount["user"][userID][type] = [time];
    if(!this.pingCount["guilds"][serverID]) this.pingCount["guilds"][serverID] = {};
    (this.pingCount["guilds"][serverID][type]) ? this.pingCount["guilds"][serverID][type].push(time) : this.pingCount["guilds"][serverID][type] = [time];
    fs.writeFileSync(pingCountPath, JSON.stringify(this.pingCount, null, 2));
};

/**
 * Get all saved counters
 * @param userID
 * @param serverID
 * @return {{user: object, guild: object}}
 */
module.exports.getPings = (userID, serverID)=>{
    return {
        user: this.pingCount["user"][userID],
        guild: this.pingCount["guilds"][serverID]
    };
};

/**
 * Resolves a id or string to a User or GuildMember Object (if possible)
 * @param {id|string|object} guild
 * @param {id|string|object} member
 * @return {*}
 */
module.exports.resolveMember = (guild, member)=>{
    if(!guild || !member) return null;
    // Todo Change to Filter & give selection Option
    return guild.members.get(member) || guild.members.find(m=>{
        const match = member.match(/<@!?(\d{17,19})>/);
        if(match && m.id === match[1]) return true;
        const username = m.user.username.toLowerCase();
        const displayName = m.displayName.toLowerCase();
        return displayName.includes(member.toLowerCase()) || username.includes(member.toLowerCase());
    });
};