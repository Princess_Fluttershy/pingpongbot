let nodeArgs = process.argv.slice(2);
console.log("\x1b[33m" + __filename.split("\\").pop().split("/").pop() + " is running now, with node parameter: " + nodeArgs + "\x1b[0m");

const path = require("path");
const fs = require("fs");
const {Client} = require("discord.js");
const util = require(path.join(__dirname, "util", "utilFunctions.js"));
util.log("- - Bot Started - -");

const bot = new Client({
    "messageCacheMaxSize": 15,
    "messageCacheLifetime": 1200,
    "messageSweepInterval": 1800,
    "fetchAllMembers": false,
    "disableEveryone": true,
    "restTimeOffset": 1250,
    "restWsBridgeTimeout": 10700,
    "disabledEvents": [
        "TYPING_START",
        "GUILD_UPDATE",
        "GUILD_MEMBER_ADD",
        "GUILD_MEMBER_REMOVE",
        "GUILD_MEMBER_UPDATE",
        "GUILD_ROLE_CREATE",
        "GUILD_ROLE_DELETE",
        "GUILD_ROLE_UPDATE",
        "GUILD_BAN_ADD",
        "GUILD_BAN_REMOVE",
        "CHANNEL_CREATE",
        "CHANNEL_DELETE",
        "CHANNEL_UPDATE",
        "CHANNEL_PINS_UPDATE",
        "MESSAGE_DELETE",
        "MESSAGE_UPDATE",
        "MESSAGE_DELETE_BULK",
        "MESSAGE_REACTION_ADD",
        "MESSAGE_REACTION_REMOVE",
        "MESSAGE_REACTION_REMOVE_ALL",
        "USER_NOTE_UPDATE",
        //"USER_SETTINGS_UPDATE"
        "RELATIONSHIP_ADD",
        "RELATIONSHIP_REMOVE"
    ]
});

bot.once("ready", ()=>{
    util.log("Bot Ready after Starting");
    console.log(`Logged in as: ${bot.user.tag}` + ` ~ running on  ${bot.guilds.size} servers`);
});

process.on("uncaughtException", err=>{
    console.log(err.stack || err);
    util.log(err.stack.toString() || err.toString());
});

function handleSignals(signal){
    if(signal === "SIGINT"){
        util.log("!(" + signal + ")[130]~! Exit-Signal.");
        process.exit(130);
    }else if(signal === "SIGTERM"){
        util.log("!(" + signal + ")[143]~! Exit-Signal.");
        process.exit(143);
    }else if(signal === "SIGQUIT"){
        util.log("!(" + signal + ")[131]~! Exit-Signal.");
        process.exit(131);
    }
}

process.on('SIGINT', handleSignals);
process.on('SIGTERM', handleSignals);
process.on('SIGQUIT', handleSignals);

/**
 * Loads all js files from a folder as single events
 * @param {object} b
 * @param {string} curPath
 * @return {*}
 */
function eventLoader(b, curPath){
    if(!fs.existsSync(curPath)) return util.log("Error while Loading events!");
    try{
        const files = fs.readdirSync(curPath);
        if(files.length === 0) return;
        let loadedEvents = "Loaded Events♦ ";
        for(let evt of files){
            if(!evt.endsWith('.js')) return;
            const event = require(path.join(curPath, evt));
            const eventName = evt.split('.')[0];
            b.on(eventName, event.bind(null, b));
            delete require.cache[require.resolve(path.join(curPath, evt))];
            loadedEvents += ` '${eventName}' ¦`;
        }
        util.log(loadedEvents);
    }catch(err){
        util.log(err);
        console.log(err);
    }
}

eventLoader(bot, path.join(__dirname, "events"));

// Timeout to prevent login-spam || login while the process is loading
// And I like that, idk why :D
setTimeout(()=>{
    bot.login(util.login.token)
        .catch(err=>console.error(err));
}, 1500);