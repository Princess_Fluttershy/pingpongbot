const path = require("path");
const {log} = require(path.join(__dirname, "..", "util", "utilFunctions.js"));

module.exports = (bot, args)=>{
    log(`BotClient on 'resume' (${bot.user.tag})` + ` ~ ${args}`);
};