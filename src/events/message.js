const moment = require("moment");
const Discord = require("discord.js");
const fs = require("fs");
const path = require("path");
const {config, log, login, RunUptime, formatTime, pingUp, resolveMember, getPings} = require(path.join(__dirname, "..", "util", "utilFunctions.js"));

const reactionsPath = path.join(__dirname, "..", "settings", "reactions.json");
const funReactions = fs.existsSync(reactionsPath) ? JSON.parse(fs.readFileSync(reactionsPath)) : {fun: [":ping_pong:"]};
// https://regex101.com/r/KiDlx4/1
// https://regex101.com/r/KiDlx4/2
const dotRegex = /^(?<all>(?<special1>[\x20-\x40\x5B-\x60\x7B-\x7E\s]*)(?<keywords>dots?|punkt|point|ping|pong|mlp\.+)(?<special2>[\x20-\x40\x5B-\x60\x7B-\x7E\s]*))+$/im;
const mentions = /<@!?\d{17,21}>/mgs;

async function pingpong(bot, message){
    let msg = message.content.toLowerCase();
    // Some Tag Words
    if(!message.channel.nsfw && (msg.includes("fuck") || msg.includes("fick") || msg.includes("dildo")))
        pingUp(message.author.id, (message.guild ? message.guild.id : null), "fu**");
    if(msg.includes("mlp") || msg.includes("pony") || msg.includes("friendship"))
        pingUp(message.author.id, (message.guild ? message.guild.id : null), "MyLittlePony♥");
    if(msg.includes("pika") || msg.includes("pikachu") || msg.includes("pokemon") || msg.includes("pokémon"))
        pingUp(message.author.id, (message.guild ? message.guild.id : null), "PikaPika");
    if(msg.includes("discord") || msg.includes("hack week") || msg.includes("#hackweek") || msg.includes("#DiscordHackWeek"))
        pingUp(message.author.id, (message.guild ? message.guild.id : null), "#DiscordHackWeek");
    if(msg.includes("<@446340148137820160>") || msg.includes("<@466205962881990667>") || msg.includes("<@570937245742268424>") || msg.includes("<@349966335352242187>") || msg.includes("<@594309099047616533> :heart"))
        pingUp(message.author.id, (message.guild ? message.guild.id : null), "💖");
    for(let i = 0; i < (msg.match(mentions)||"").length; i++)
        pingUp(message.author.id, (message.guild ? message.guild.id : null), "Mentions");
    // Normal & Fun
    if(message.author.bot){
        // Something Special for Bots ...
        return;
    }
    // Reactions/Responses
    if(message.content === ":ping_pong:" || message.content === "🏓" || msg.startsWith("<@594309099047616533>")){
       pingUp(message.author.id, (message.guild ? message.guild.id : null), "Ping");
        return message.channel.send(":ping_pong:");
    }else if(msg.startsWith("ping")){
       pingUp(message.author.id, (message.guild ? message.guild.id : null), "Ping");
        return message.reply("PONG!");
    }else if(msg.startsWith("pong")){
       pingUp(message.author.id, (message.guild ? message.guild.id : null), "Ping");
        return message.reply("Ping?");
    }else if(msg.includes("ping") || msg.includes("pong") || msg.includes("<@594309099047616533>")){
       pingUp(message.author.id, (message.guild ? message.guild.id : null), "Ping");
        message.react('🏓').catch(err=>{
            bot.log = "FunReactions(Ping_Pong): [" + err.code + "] " + err;
        });
    }

    if(dotRegex.exec(message.content) !== null){
        if(Math.floor(Math.random()*6) > 4) return;
        let emote = funReactions.fun[Math.floor(Math.random()*funReactions.fun.length)];
        pingUp(message.author.id, (message.guild ? message.guild.id : null), "funDot");
        message.react(emote).catch(err=>{
            if(err.code === 10014){
                try{
                    let index = funReactions.fun.indexOf(emote);
                    funReactions.fun.splice(index, 1);
                    fs.writeFileSync(reactionsPath, JSON.stringify(funReactions, null, 2));
                }catch(err){
                    log(err);
                    console.log("#Error# ~ Please view the Logs!");
                }
            }
            bot.log = "(MLP)FunReactions (dot " + emote + "): [" + err.code + "] " + err;
        });
    }

    if(message.content.indexOf("eWF5") !== -1 || msg.indexOf("lnl!~") !== -1 || msg.indexOf("wwf5ix4=") !== -1 || msg.indexOf("-.-- .- -.--") !== -1){
        pingUp(message.author.id, (message.guild ? message.guild.id : null), "yay");
        await message.react(':yay:424683278336000011').catch(err=>{
            bot.log = "FunReactions(yay): [" + err.code + "] " + err;
        });
    }
}

module.exports = async(bot, message)=>{
    if(!message || !message.author || message.author.id === bot.user.id) return;
    // Check Message and split into command, invoke, args
    if(message.author.bot){
        return pingpong(bot, message).catch(err=>log(err));
    }
    if(!message.content.toLowerCase().startsWith(config.prefix) || message.content.length <= config.prefix.length){
        return pingpong(bot, message).catch(err=>log(err));
    }

    /**
     * Arguments without invoke
     * * message.content.split(' ')
     * @type {*|string[]}
     */
    let args = message.content.split(' ');

    /**
     * The invoke (prefix + original Command)
     * @type {*|string}
     */
    let invoke = args[0];

    /**
     * The command input
     * @type {string}
     */
    let command;

    // true = space between prefix + command
    if(invoke.toLowerCase() === config.prefix){
        command = message.content.split(' ')[1].toLowerCase();
        invoke = invoke + args[1];
        args = args.slice(2);
    }else{
        command = invoke.substr(config.prefix.length).toLowerCase();
        args = args.slice(1);
    }

    // Command Handling
    let pingAliases = ["ping", "pong", "up", "uptime", "p?", "ping_pong", ":ping_pong", "👋", ":ping_pong:", "🏓", "runtime"];
    if(pingAliases.indexOf(command) > -1){
        pingUp(message.author.id, (message.guild ? message.guild.id : null), "PingCmd");
        return message.reply(`:ping_pong: Pong! \`${bot.pings[0]}ms\`\nUptime: \`${formatTime(bot.uptime)}\`\nRuntime: \`${formatTime(moment().format("x") - RunUptime)}\``);
    }else if(command === "invite"){
        let embed = new Discord.RichEmbed()
            .setThumbnail(bot.user.avatarURL)
            .addField("Discord", "[discord.gg/kkTcW5z](https://discord.gg/kkTcW5z)")
            .addField("Invite", `[${bot.user.username}](https://discordapp.com/oauth2/authorize?client_id=${bot.user.id}&permissions=1312156993&scope=bot)`);

        if(message.author.id === login.ownerID && args[0] === "-all"){
            embed.addField("Owner ~ Invites", "• [FlutterBot](https://discordapp.com/oauth2/authorize?client_id=446340148137820160&permissions=2146958847&scope=bot)\n• [InfoBot](https://discordapp.com/oauth2/authorize?client_id=466205962881990667&permissions=2146958847&scope=bot)\n• [UltraBot](https://discordapp.com/oauth2/authorize?client_id=513710662380290048&permissions=2146958847&scope=bot)");
        }
        embed.setFooter(`requested by ${message.author.tag}, using ${invoke}`, message.author.avatarURL);
        return message.reply(embed);
    }else if(command === "leavevc" || command === "dc" || command === "disconnect" || command === "leave" || command === "leavevc" || command === "leavevoice"){
        if(message.guild.voiceConnection){
            message.guild.voiceConnection.channel.leave();
            return message.reply(":ok_hand:");
        }else{
            return message.reply(":x: I'm not connected to a voice channel in this guild!");
        }
    }else if(command === "say" || command === "speak" || command === "respond" || command === "sayd" || command === "speakd" || command === "reply" || command === "echo"){
        if(args[0] === undefined) return;
        // Say und Saydelete
        if(command === 'sayd' || command === 'speakd'){
            //if(message.author.id === bot.config.owner || message.member.hasPermission("MANAGE_MESSAGES")) {
            message.delete([5])
                .catch(err=>{
                    log("MessageDeletion: [" + err.code + "] " + err);
                });
            //}
        }
        let cont = message.content.toLowerCase();
        if(cont.includes("ping") || cont.includes("pong")) pingUp(message.author.id, (message.guild ? message.guild.id : null), "SayPing");
        return message.channel.send(args.join(' ')).catch(err=>{
            log(err);
        });
    }else if(command === "about" || command === "owner" || command === "support" || command === "engel" || command === "fluttershy" || command === "help" || command === "hack"|| command === "git"|| command === "gitlab" || command === "repo" || command === "link"){
        let embed = new Discord.RichEmbed()
            .setColor(0x297688);
        if(message.author.id === config.ownerID && args[0] === "-dev"){
            embed.addField("Hello Dev :wave:", "Everything good? :)\n" +
                `• Ping: **${formatTime(bot.pings[0])}**\n• Uptime: **${formatTime(bot.uptime, {"long": true})}**\n• Prefix: **${config.prefix}**`);
            embed.setAuthor(config.ownerTag.slice(0, config.ownerTag.length - 5), bot.user.avatarURL, `https://discordapp.com/oauth2/authorize?client_id=${bot.user.id}&permissions=2146958847&scope=bot`);
        }

        embed.setTitle(`__**About Me, ${bot.user.username}, and my Developer**__`)
            .setDescription(`**${config.ownerTag.slice(0, config.ownerTag.length - 5)}**\`${(config.ownerTag.slice(config.ownerTag.length - 5))}\` created me during the [Discord Hack Week 2019](https://blog.discordapp.com/discord-community-hack-week-build-and-create-alongside-us-6b2a7b7bba33) :)"+config.prefix+"\nIf you want to support my developer, feel free to:`)
            .addField("ℹ **Join the official Discord**", "- Get support and help\n- Report bugs and __request features__\n- Help us to grow up:" +
                "\n[🔗 **https://discord.gg/GEJ5YqD**](https://discord.gg/GEJ5YqD)", true)
            .addField("⏬ **Invite Me to your server**", `[Click me! ${bot.user.username}](https://discordapp.com/oauth2/authorize?client_id=${bot.user.id}&permissions=1312156993&scope=bot)`, true)
            .addField("📁 **View My Code on GitLab**", `[GitLab Repository](https://gitlab.com/Princess_Fluttershy/pingpongbot)`, true)
            .setThumbnail(bot.user.avatarURL)
            .addField("💠 **Some Commands**",
                "* `"+config.prefix+"help` | `"+config.prefix+"about` - Shows a little Info, Bot Invite & Server Invite.\n" +
                "* `"+config.prefix+"ping` | `"+config.prefix+"up` - Shows the current API-Ping, Uptime & Runtime.\n" +
                "* `"+config.prefix+"profile <@user|id|name>` | `"+config.prefix+"check <@user|id|name>` - Get a users profile & the server Profile; It shows how often a user said \"ping\" and some other secret tags.\n" +
                "* `"+config.prefix+"say <text>` - I will send what you sended, but without the cmd.\n" +
                "* `"+config.prefix+"sayd <text>` - Same as above, but I will delete your message if I have permissions to.")
            .setFooter(`requested by ${message.author.tag}, using ${invoke}`, message.author.avatarURL);
        return message.reply(embed);
    }else if(command === "profile" || command === "check"){
        let user = message.mentions.users.first();
        if(!user && message.guild && args[0]){
            user = resolveMember(message.guild, args[0]);
        }
        if(!user) user = message.author;
        let pingStats = getPings(user.id, message.guild.id);
        let userStr = "";
        if(pingStats.user){
            for(let value in pingStats.user)
                if(pingStats.user.hasOwnProperty(value))
                    userStr += "• " + value + ": " + pingStats.user[value].length + "\n";
        }else userStr = "None";

        let guildStr = "";
        if(pingStats.guild){
            for(let value in pingStats.guild)
                if(pingStats.guild.hasOwnProperty(value))
                    guildStr += "• " + value + ": " + pingStats.guild[value].length + "\n";
        }else guildStr = "None";
        let embed = new Discord.RichEmbed()
            .setColor(0x297688)
            .setFooter(`requested by ${message.author.tag}, using ${invoke}`, message.author.avatarURL)
            .addField((user.username || user.user.username) + "'  Profile", userStr, true);
        if(message.guild)
            embed.addField(`${message.guild.name}'  Stats`, guildStr, true);
        return message.channel.send("<@" + message.author.id + ">", embed);
    }

    pingpong(bot, message).catch(err=>log(err));

    // Owner only Section
    if(message.author.id !== login.ownerID) return;
    if(command === "stats"){
        let embed = new Discord.RichEmbed()
            .setColor(0x297688)
            .setTimestamp()
            .setFooter(`requested by ${message.author.tag}, using ${invoke}`, message.author.avatarURL)
            .setThumbnail(bot.user.avatarURL)
            .setTitle("Stat's of " + bot.user.username + ` ; Owner: ${config.ownerTag}`)
            .addField(`Guilds ¦ Members (Loaded)`, `**${bot.guilds.size} ¦ ${bot.users.size}**`, true)
            .addField(`BotStatus`, `**${config.status}**`, true)
            .addField(`GameStatus`, `${bot.user.presence.game}`, true)
            .addField(`Info`, `• Version: **${config.version}**\n• Owner: **${config.ownerTag}**\n• Uptime: **${formatTime(bot.pings[0])}**\n• Runtime: **${formatTime(moment().format("x") - RunUptime, {"long": true})}**\n• Prefix: **${config.prefix}**`);
        return message.channel.send(embed);
    }else if(command === "prefix"){
        if(!args[0]) return;
        config.prefix = args[0];
        fs.writeFileSync(path.join(__dirname, "..", "settings/config.json"), JSON.stringify(config, null, 2), "UTF-8");
        message.reply("<:success:532701322877599755> Done - new Prefix: `" + config.prefix + "`");
    }else if(command === "exit" && message.author.id === login.ownerID){
        message.channel.send(`:arrows_counterclockwise: Restarting...`);
        message.react('🔰').catch((err)=>{
            log(err);
        });
        setTimeout(()=>{
            bot.destroy().catch((err)=>{
                log(err);
            });
        }, 300);
    }else if(command === "setconfig" || command === "config"){
        let reply = "Loading config ....";
        let newNum = parseFloat(args[1]);
        let changed = true;
        if(args[0]){
            switch(args[0].toLowerCase()){
                case "version":
                    isNaN(newNum) ? reply += "\n<:report:532701488569647124:> Couldn't parse Float [`" + args[1] + "` ~ `" + newNum + "`]" : config.version = newNum;
                    break;
                case "owner":
                case "ownertag":
                    config.ownerTag = message.author.tag;
                    break;
                case "status":
                    config.status = args.slice(1).join(" ");
                    break;
                case "reconnectmax":
                case "reconnectstrike":
                case "reconnectstrikethreshold":
                    isNaN(newNum) ? reply += "\n<:report:532701488569647124:> Couldn't parse Float [`" + args[1] + "` ~ `" + newNum + "`]" : config.reconnectStrikeThreshold = newNum;
                    break;
                case "rreconnectmax":
                case "recoverreconnectmax":
                case "recoverreconnectstrike":
                case "recoverreconnectstrikethreshold":
                    isNaN(newNum) ? reply += "\n<:report:532701488569647124:> Couldn't parse Float [`" + args[1] + "` ~ `" + newNum + "`]" : config.recoverReconnectStrikeThreshold = newNum;
                    break;
                case "gameadd":
                case "gamesadd":
                    config.games.push(args.slice(1).join(" ").slice(0, 15));
                    break;
                case "gamedel":
                case "gameremove":
                case "gamesdel":
                case "gamesremove":
                    newNum = parseInt(args[1]);
                    isNaN(newNum) ? reply += "\n<:report:532701488569647124:> Couldn't parse Int [`" + args[1] + "` ~ `" + newNum + "`]" : config.games.splice(newNum, 1);
                    break;
                default:
                    reply += "\n<:404notfound:532701322609426451:> I couldn't find `" + args[0] + "`";
                    changed = false;
            }
            if(changed){
                await fs.writeFileSync(path.join(__dirname, "..", "settings/config.json"), JSON.stringify(config, null, 2), "UTF-8");
                reply += "\n<:success:532701322877599755:> Changed `" + args[0] + "` to `" + (isNaN(newNum) ? args.slice(1).join(" ") : newNum) + "`";
            }
        }
        reply += "\n**__Current Config__**\n";
        reply += `Prefix: \`${config.prefix}\`\nVersion: \`${config.version}\`\nStatus: \`${config.status}\`\nownerTag: \`${config.ownerTag}\`\nGames: \`${config.games.join("` ¦ `")}\`\nreconnectStrikeThreshold: \`${config.reconnectStrikeThreshold}\``; // \nrecoverReconnectStrikeThreshold: \`${config.recoverReconnectStrikeThreshold}\`
        return message.reply(reply);
    }
};