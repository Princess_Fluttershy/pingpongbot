const path = require("path");
const {log} = require(path.join(__dirname, "..", "util", "utilFunctions.js"));

module.exports = (bot, error)=>{
    console.log("Error(event): [" + error.code + "]" + error.toString());
    log("Error(event): [" + error.code + "]" + error.toString());
};