const path = require("path");
const {log} = require(path.join(__dirname, "..", "util", "utilFunctions.js"));

module.exports = (bot, guild)=>{
    log(`Joined ${guild} ¦ new Bot-Server-count: ${bot.guilds.size}`);
};