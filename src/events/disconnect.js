const path = require("path");
const {log} = require(path.join(__dirname, "..", "util", "utilFunctions.js"));

module.exports = (bot)=>{
    log(`!! ~ Disconnected as: ${bot.user.username}`);
};