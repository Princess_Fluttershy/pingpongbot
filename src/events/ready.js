const path = require("path");
const {log, config} = require(path.join(__dirname, "..", "util", "utilFunctions.js"));

let failedChanges = 0;

module.exports = async(bot)=>{
    await log(`- Logged in as: ${bot.user.tag}` + ` ~ running on  ${bot.guilds.size} servers`);
    let status = config.games;
    status.push(`${config.prefix}help`, config.version, `${config.ownerTag} is best 💖`, `${config.prefix}about`);
    let readyIntervalTime = Math.round(55555*((10/status.length) + 0.6));
    bot.setTimeout(function statusFunction(){
        let gameActivity = status[Math.floor(Math.random()*status.length)];
        if(gameActivity.length <= 12) gameActivity += " ¦ " + status[Math.floor(Math.random()*status.length)];
        if(failedChanges > 0) failedChanges--;
        bot.user.setPresence({game: {name: gameActivity, type: 0}, status: 'online'}).catch(error=>{
            failedChanges += 3;
            log("⚠ Couldn't change botUser Presence (FailedChanges: " + failedChanges + ") Err: " + error);
        });
        bot.setTimeout(statusFunction, readyIntervalTime + (failedChanges*3000) + Math.round(Math.random()*500));
    }, readyIntervalTime + 7500);
};