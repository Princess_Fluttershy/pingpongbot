const path = require("path");
const {log} = require(path.join(__dirname, "..", "util", "utilFunctions.js"));

let reconnectCount = 0;
let reduceReconnectCountTimeout = null;

function reduceReconnectCount(curClient){
    reduceReconnectCountTimeout = null;
    if(reconnectCount < 1) return;
    reconnectCount--;
    if(reconnectCount > 0){
        reduceReconnectCountTimeout = curClient.setTimeout(reduceReconnectCount, 45000, curClient);
    }
}


module.exports = (bot)=>{
    log(`Reconnected as: ${bot.user.tag}` + ` ~ running on ${bot.guilds.size} servers`);
    reconnectCount++;
    if(reconnectCount > util.config.reconnectStrikeThreshold){
        log(`ReconnectCount: ` + reconnectCount + " ~ Cleaning cache..");
        let n = bot.sweepMessages(150);
        log("Removed " + n + " messages from the cache");
        if(reconnectCount > util.config.reconnectStrikeThreshold*2)
            bot.destroy();
    }
    if(reduceReconnectCountTimeout === null) reduceReconnectCountTimeout = bot.setTimeout(reduceReconnectCount, 15000, bot);
};
