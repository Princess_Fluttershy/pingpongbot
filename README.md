__**PingPongBot**__

A Discord Fun Bot for the [Discord Hack Week 2019](https://blog.discordapp.com/discord-community-hack-week-build-and-create-alongside-us-6b2a7b7bba33).

Actually I wanted todo a different Bot, but then things changed. And I didn't have much time for this, so Ping? - Pong!#0744 is pretty small.

I worked 4-5 hours on the other Bot and I got: A good system, multiple language support, modules, many util functions and other stuff. 
But it had no events or commands except PingPong & some defaults. As I have a lot of moderation plans I decided to work on the moderation Bot over the next weeks and submit another Bot to the [Discord Hack Week 2019](https://blog.discordapp.com/discord-community-hack-week-build-and-create-alongside-us-6b2a7b7bba33).
And when I told others about my system and that the only thing the Bot can do is Ping-Pong they were excited! Yeah, really! They wanted Ping-Pong!
Now you have it! A Bot just for Ping-Pong *~~and some little secrets~~*! 
Have fun ;D

* I will host the Bot, so you don't need to run it yourself.
* Invite the Bot: [Ping? - Pong!#0744 Invite - Select](https://discordapp.com/oauth2/authorize?client_id=594309099047616533&permissions=1312156993&scope=bot)
* *Ping? - Pong!#0744 is now 24/7 Online!*

__Some Commands__
* `ping!help` | `ping!about` - Shows a little Info, Bot Invite & Server Invite.
* `ping!ping` | `ping!up` - Shows the current API-Ping, Uptime & Runtime.
* `ping!profile <@user|id|name>` - Get a users profile & the server Profile; It shows how often a user said "ping" and some other secret tags.
* `ping!say <text>` - I will send what you sended, but without the cmd.
* `ping!sayd <text>` - Same as above, but I will delete your message if I have permissions to.
* There are also some owner-only commands. E.g. to change the config-file (Means: Games, Limits, Status, Prefix etc. - You can't change the login!).


If you can find a bug, then please open a issue (there shouldn't be any bugs).

Copyright 2019 engel_pika#3979 (176335473977851905)